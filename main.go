package main

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"fmt"
	"log"
	"math"
	"math/rand"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"
)

func matrix(row, col int) [][]float64 {
	m := make([][]float64, row)
	for i := 0; i < row; i++ {
		m[i] = make([]float64, col)
	}
	return m
}
func vector(l int, fill float64) []float64 {
	v := make([]float64, l)
	for i := 0; i < l; i++ {
		v[i] = fill
	}
	return v
}
func sigmoid(x float64) float64   { return 1 / (1 + math.Exp(-x)) }
func dsigmoid(y float64) float64  { return y * (1 - y) }
func random(a, b float64) float64 { return (b-a)*rand.Float64() + a }

type NeuralNetwork struct {
	nInputs, nHiddens, nOutputs                           int
	inputWeights, outputWeights                           [][]float64
	inputChanges, outputChanges                           [][]float64
	inputActivations, hiddenActivations, ouputActivations []float64
}

func NewNetwork(inputs, hiddens, outputs int) *NeuralNetwork {
	nn := new(NeuralNetwork)
	nn.Init(inputs, hiddens, outputs)
	return nn
}
func (nn *NeuralNetwork) Init(inputs, hiddens, outputs int) {
	nn.nInputs = inputs
	nn.nHiddens = hiddens
	nn.nOutputs = outputs
	nn.inputActivations = vector(nn.nInputs, 1.0)
	nn.hiddenActivations = vector(nn.nHiddens, 1.0)
	nn.ouputActivations = vector(nn.nOutputs, 1.0)

	nn.inputWeights = matrix(nn.nInputs, nn.nHiddens)
	nn.outputWeights = matrix(nn.nHiddens, nn.nOutputs)
	for i := 0; i < nn.nHiddens; i++ {
		for j := 0; j < nn.nOutputs; j++ {
			nn.outputWeights[i][j] = random(-1, 1)
		}
		for j := 0; j < nn.nInputs; j++ {
			nn.inputWeights[j][i] = random(-1, 1)
		}
	}

	nn.inputChanges = matrix(nn.nInputs, nn.nHiddens)
	nn.outputChanges = matrix(nn.nHiddens, nn.nOutputs)
}

func (nn *NeuralNetwork) GetOut(inputs []float64) []float64 {
	for i := 0; i < nn.nInputs; i++ {
		nn.inputActivations[i] = inputs[i]
	}
	for i := 0; i < nn.nHiddens; i++ {
		var sum float64
		for j := 0; j < nn.nInputs; j++ {
			sum += nn.inputActivations[j] * nn.inputWeights[j][i]
		}
		nn.hiddenActivations[i] = sigmoid(sum)
	}
	for i := 0; i < nn.nOutputs; i++ {
		var sum float64
		for j := 0; j < nn.nHiddens; j++ {
			sum += nn.hiddenActivations[j] * nn.outputWeights[j][i]
		}
		nn.ouputActivations[i] = sigmoid(sum)
	}
	return nn.ouputActivations
}
func (nn *NeuralNetwork) teach(targets []float64, lRate, mFactor float64) float64 {
	outputDeltas := vector(nn.nOutputs, 0.0)
	for i := 0; i < nn.nOutputs; i++ {
		outputDeltas[i] = dsigmoid(nn.ouputActivations[i]) * (targets[i] - nn.ouputActivations[i])
	}
	hiddenDeltas := vector(nn.nHiddens, 0.0)
	for i := 0; i < nn.nHiddens; i++ {
		var e float64
		for j := 0; j < nn.nOutputs; j++ {
			e += outputDeltas[j] * nn.outputWeights[i][j]
		}
		hiddenDeltas[i] = dsigmoid(nn.hiddenActivations[i]) * e
	}
	for i := 0; i < nn.nHiddens; i++ {
		for j := 0; j < nn.nOutputs; j++ {
			change := outputDeltas[j] * nn.hiddenActivations[i]
			nn.outputWeights[i][j] = nn.outputWeights[i][j] + lRate*change + mFactor*nn.outputChanges[i][j]
			nn.outputChanges[i][j] = change
		}
	}
	for i := 0; i < nn.nInputs; i++ {
		for j := 0; j < nn.nHiddens; j++ {
			change := hiddenDeltas[j] * nn.inputActivations[i]
			nn.inputWeights[i][j] = nn.inputWeights[i][j] + lRate*change + mFactor*nn.inputChanges[i][j]
			nn.inputChanges[i][j] = change
		}
	}
	var e float64
	for i := 0; i < nn.nOutputs; i++ {
		e += math.Pow(targets[i]-nn.ouputActivations[i], 2)
	}
	return e / float64(nn.nOutputs)
}
func (nn *NeuralNetwork) test(targets []float64) float64 {
	var e float64
	for i := 0; i < nn.nOutputs; i++ {
		e += math.Pow(targets[i]-nn.ouputActivations[i], 2)
	}
	return e / float64(nn.nOutputs)
}
func (nn *NeuralNetwork) Success(in, out [][]float64, treshhold float64) float64 {
	var e float64
	for ind, _ := range in {
		output := nn.GetOut(in[ind])
		for i := 0; i < nn.nOutputs; i++ {
			if math.Abs(out[ind][i]-output[i]) > treshhold {
				e += 1.0
			}
		}
	}
	return 1 - e/float64(nn.nOutputs)/float64(len(in))
}
func (nn *NeuralNetwork) Test(in, out [][]float64) {
	for i, _ := range in {
		fmt.Printf("%.6f %.6f %.6f\n", out[i], nn.GetOut(in[i]), nn.test(out[i]))
	}
}
func (nn *NeuralNetwork) Train(in, out [][]float64, iteration int, lRate, mFactor float64, debug bool) []float64 {
	errors := make([]float64, iteration)
	for i := 0; i < iteration; i++ {
		var e float64
		for i, _ := range in {
			nn.GetOut(in[i])
			e += nn.teach(out[i], lRate, mFactor)
		}
		errors[i] = e
		if debug && (i%(iteration/20) == 0) {
			fmt.Println(i, e)
		}
	}
	return errors
}
func ErrAndExit(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

type NetworkData struct {
	NIN   []bool
	OK    float64
	Learn float64
	len   int
	hash  string
}

func (nd *NetworkData) Len() int {
	if nd.len == 0 {
		for _, ok := range nd.NIN {
			if ok {
				nd.len++
			}
		}
	}
	return nd.len
}
func (nd *NetworkData) Hash() string {
	if nd.hash == "" {
		for _, ok := range nd.NIN {
			if ok {
				nd.hash += "1"
			} else {
				nd.hash += "0"
			}
		}
	}
	return nd.hash
}
func (nd *NetworkData) GetInfo() string {
	buffer := bytes.NewBufferString("")
	buffer.WriteString(fmt.Sprintf("%f %f %d =", nd.OK, nd.Learn, nd.Len()))
	for i, ok := range nd.NIN {
		if ok {
			buffer.WriteString(fmt.Sprintf(" %d", i+1))
		}
	}
	return buffer.String()
}

func NewNetworkData(size int) *NetworkData {
	n := new(NetworkData)
	n.NIN = make([]bool, size)
	return n
}

type NetworkList struct {
	networkList                              []*NetworkData
	Size                                     int
	NINSize                                  int
	MutationChance                           float64
	inData, outData, inDataTest, outDataTest [][]float64
	visited                                  map[string]bool
}

func (nl *NetworkList) AddNetwork(network *NetworkData) {
	if _, ok := nl.visited[network.Hash()]; !ok {
		nl.networkList = append(nl.networkList, network)
		nl.visited[network.Hash()] = true
	}
}
func (nl *NetworkList) Trim() {
	if len(nl.networkList) > nl.Size {
		nl.networkList = nl.networkList[:nl.Size]
	}
}
func (nl *NetworkList) Init() {
	nl.visited = make(map[string]bool)
	child1 := NewNetworkData(nl.NINSize)
	child2 := NewNetworkData(nl.NINSize)
	child3 := NewNetworkData(nl.NINSize)
	child4 := NewNetworkData(nl.NINSize)
	child5 := NewNetworkData(nl.NINSize)
	for i := 0; i < nl.NINSize; i++ {
		if rand.Float64() > 0.5 {
			child3.NIN[i] = true
		} else {
			child3.NIN[i] = false
		}
		if rand.Float64() > 0.5 {
			child4.NIN[i] = true
		} else {
			child4.NIN[i] = false
		}
		child5.NIN[i] = true
		if i%2 == 0 {
			child1.NIN[i] = true
			child2.NIN[i] = false
		} else {
			child1.NIN[i] = false
			child2.NIN[i] = true
		}
	}
	child1.OK = nl.getResult(child1.NIN)
	child2.OK = nl.getResult(child2.NIN)
	child3.OK = nl.getResult(child3.NIN)
	child4.OK = nl.getResult(child4.NIN)
	child5.OK = nl.getResult(child5.NIN)
	child1.Learn = nl.getLearnResult(child1.NIN)
	child2.Learn = nl.getLearnResult(child2.NIN)
	child3.Learn = nl.getLearnResult(child3.NIN)
	child4.Learn = nl.getLearnResult(child4.NIN)
	child5.Learn = nl.getLearnResult(child5.NIN)
	nl.AddNetwork(child1)
	nl.AddNetwork(child2)
	nl.AddNetwork(child3)
	nl.AddNetwork(child4)
	nl.AddNetwork(child5)
}
func (nl *NetworkList) NewChilds() {
	parrent1 := nl.networkList[0]
	parrent2 := nl.networkList[1]
	nlen := len(nl.networkList)
	parrent3 := nl.networkList[rand.Intn(nlen-1)+1]
	child1 := NewNetworkData(nl.NINSize)
	child2 := NewNetworkData(nl.NINSize)
	child3 := NewNetworkData(nl.NINSize)
	for i := 0; i < nl.NINSize; i++ {
		if rand.Float64() > 0.5 {
			child1.NIN[i] = parrent1.NIN[i]
			child2.NIN[i] = parrent3.NIN[i]
		} else {
			child1.NIN[i] = parrent2.NIN[i]
			child2.NIN[i] = parrent1.NIN[i]
		}
		child3.NIN[i] = parrent1.NIN[i]
	}
	for i := 0; i < nl.NINSize; i++ {
		if rand.Float64() < nl.MutationChance {
			child1.NIN[i] = !child1.NIN[i]
		}
		if rand.Float64() < nl.MutationChance {
			child2.NIN[i] = !child2.NIN[i]
		}
		if rand.Float64() < nl.MutationChance {
			child3.NIN[i] = !child2.NIN[i]
		}
	}
	child1.OK = nl.getResult(child1.NIN)
	child2.OK = nl.getResult(child2.NIN)
	child3.OK = nl.getResult(child3.NIN)
	child1.Learn = nl.getLearnResult(child1.NIN)
	child2.Learn = nl.getLearnResult(child2.NIN)
	child3.Learn = nl.getLearnResult(child3.NIN)
	nl.AddNetwork(child1)
	nl.AddNetwork(child2)
	nl.AddNetwork(child3)
}
func (nl *NetworkList) getResult(NIN []bool) float64 {
	in := make([][]float64, 0)
	out := make([][]float64, 0)
	for i, ok := range NIN {
		if ok {
			in = append(in, nl.inData[i])
			out = append(out, nl.outData[i])
		}
	}
	return getNetworkResult(in, out, nl.inDataTest, nl.outDataTest)
}
func (nl *NetworkList) getLearnResult(NIN []bool) float64 {
	in := make([][]float64, 0)
	out := make([][]float64, 0)
	for i, ok := range NIN {
		if ok {
			in = append(in, nl.inData[i])
			out = append(out, nl.outData[i])
		}
	}
	return getNetworkResult(in, out, nl.inData, nl.outData)
}
func (nl *NetworkList) Less(i, j int) bool {
	if nl.networkList[i].OK == nl.networkList[j].OK {
		if nl.networkList[i].Learn == nl.networkList[j].Learn {
			return nl.networkList[i].Len() < nl.networkList[j].Len()
		} else {
			return nl.networkList[i].Learn > nl.networkList[j].Learn
		}
	}
	return nl.networkList[i].OK > nl.networkList[j].OK
}
func (nl *NetworkList) Swap(i, j int) {
	nl.networkList[i], nl.networkList[j] = nl.networkList[j], nl.networkList[i]
}
func (nl *NetworkList) Len() int { return len(nl.networkList) }
func (nl *NetworkList) SetData(inData, outData, inDataTest, outDataTest [][]float64) {
	nl.inData = inData
	nl.outData = outData
	nl.inDataTest = inDataTest
	nl.outDataTest = outDataTest
}
func main() {
	rand.Seed(0)
	inData := make([][]float64, 0)
	outData := make([][]float64, 0)
	inDataTest := make([][]float64, 0)
	outDataTest := make([][]float64, 0)
	scanner := bufio.NewScanner(os.Stdin)
	file, err := os.Open("train.csv")
	ErrAndExit(err)
	reader := csv.NewReader(file)
	reader.Comma = ';'
	rows, err := reader.ReadAll()
	ErrAndExit(err)
	for _, row := range rows {
		in := make([]float64, len(row)-1)
		out := make([]float64, 1)
		for i, cell := range row {
			val, _ := strconv.ParseFloat(cell, 64)
			if i == len(row)-1 {
				out[0] = val/2 - 1
			} else {
				in[i] = val / 10
			}
		}
		inData = append(inData, in)
		outData = append(outData, out)
	}
	file, err = os.Open("test.csv")
	ErrAndExit(err)
	reader = csv.NewReader(file)
	reader.Comma = ';'
	rows, err = reader.ReadAll()
	ErrAndExit(err)
	for _, row := range rows {
		in := make([]float64, len(row)-1)
		out := make([]float64, 1)
		for i, cell := range row {
			val, _ := strconv.ParseFloat(cell, 64)
			if i == len(row)-1 {
				out[0] = val/2 - 1
			} else {
				in[i] = val / 10
			}
		}
		inDataTest = append(inDataTest, in)
		outDataTest = append(outDataTest, out)
	}
	fmt.Println("select mode\n0 - genGen\n1 - testTest")
	scanner.Scan()
	switch scanner.Text() {
	case "0":
		genGen(scanner, inData, outData, inDataTest, outDataTest)
	case "1":
		testTest(scanner, inData, outData, inDataTest, outDataTest)
	}

}
func testTest(scanner *bufio.Scanner, inData, outData, inDataTest, outDataTest [][]float64) {
	rand.Seed(time.Now().Unix())
	fmt.Println("enter test numbers")
	scanner.Scan()
	strTests := strings.Split(scanner.Text(), " ")
	nTests := make([]int, 0)
	for _, t := range strTests {
		v, _ := strconv.Atoi(t)
		nTests = append(nTests, v-1)
	}
	in := make([][]float64, 0)
	out := make([][]float64, 0)
	for _, i := range nTests {
		in = append(in, inData[i])
		out = append(out, outData[i])
	}
	for i := 0; i < 30; i++ {
		fmt.Println(i, getNetworkResult(in, out, inDataTest, outDataTest), getNetworkResult(in, out, inData, outData))
	}

}
func genGen(scanner *bufio.Scanner, inData, outData, inDataTest, outDataTest [][]float64) {
	fmt.Println("enter filename")
	scanner.Scan()
	file, err := os.Create(scanner.Text())
	if err != nil {
		log.Println(err)
	}
	fmt.Fprintln(file, "Success rate with full data", getNetworkResult(inData, outData, inDataTest, outDataTest))
	networks := new(NetworkList)
	networks.SetData(inData, outData, inDataTest, outDataTest)
	networks.Size = 50
	fmt.Println("enter mutation chance")
	scanner.Scan()
	much, _ := strconv.ParseFloat(scanner.Text(), 64)
	networks.MutationChance = much
	networks.NINSize = len(inData)
	networks.Init()
	fmt.Println("enter iteration count")
	scanner.Scan()
	iterCount, _ := strconv.Atoi(scanner.Text())
	fmt.Println("enter info len")
	scanner.Scan()
	iterInfoLen, _ := strconv.Atoi(scanner.Text())
	fmt.Fprintln(file, "Train data len", networks.NINSize)
	fmt.Fprintln(file, "Population max len", networks.Size)
	fmt.Fprintln(file, "Mutation chance", networks.MutationChance)
	fmt.Fprintln(file, "Iteration count", iterCount)
	fmt.Fprintf(file, "Format\nsuccess_percent learn_rate data_len = data numbers (1:len)\n")
	for iteration := 0; iteration < iterCount; iteration++ {
		log.Println(iteration, networks.networkList[0].Len())
		sort.Sort(networks)
		if iteration%iterInfoLen == 0 {
			fmt.Fprintln(file, "================= iteration ", iteration, " =================\n>mc =", networks.MutationChance, "\n>unique =", len(networks.visited))
			for i, net := range networks.networkList {
				if i > 9 {
					break
				}
				fmt.Fprintln(file, net.GetInfo())
			}
		}
		networks.Trim()
		networks.NewChilds()
		/*if (iteration+1)%(iterCount/10) == 0 {
			networks.MutationChance *= 0.9
		}*/
	}
	sort.Sort(networks)
	fmt.Fprintln(file, "================= FINISH =================\n>mc =", networks.MutationChance, "\n>unique =", len(networks.visited))
	for _, net := range networks.networkList {
		fmt.Fprintln(file, net.GetInfo())
	}
	log.Println("FINISHED")
}

func getNetworkResult(inData, outData, inDataTest, outDataTest [][]float64) float64 {
	network := NewNetwork(9, 30, 1)
	network.Train(inData, outData, 600, 0.6, 0.4, false)
	return network.Success(inDataTest, outDataTest, 0.2)
}
